<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('siswa/login', function(){
    return view('auth/login-siswa');
});
Route::post('siswa/login', 'Auth\LoginSiswaController@login')->name('siswa.login');
Route::post('siswa/logout', 'Auth\LoginSiswaController@logout')->name('siswa.logout');
Route::middleware(['auth:siswa'])->prefix('siswa')->group(function(){
    Route::get('dashboard', 'DashboardController@indexSiswa')->name('siswa.dashboard');
    Route::get('kelas', 'KelasController@index')->name('siswa.kelas.index');
    Route::get('kelas/{id}/siswa', 'KelasController@kelasSiswa')->name('siswa.kelas.detail');
    Route::get('pengumuman', 'PengumumanController@index')->name('siswa.pengumuman');
    Route::get('pengumuman/{id}', 'PengumumanController@show')->name('siswa.pengumuman.show');

    Route::get('materi-tugas', 'MateriTugasController@index')->name('siswa.materi-tugas.index');
    Route::get('materi-tugas/{id}', 'MateriTugasController@show')->name('siswa.materi-tugas.show');
    Route::post('materi-tugas/{id}', 'MateriTugasController@submitJawaban')->name('siswa.jawaban');
});

Route::get('/', function () {
    return view('auth/login-siswa');
});

Auth::routes();

Route::middleware(['auth:web'])->group(function(){
    Route::get('dashboard', function(){
        return view('dashboard');
    });
    Route::resource('kelas', 'KelasController');
    Route::resource('siswa', 'SiswaController')->middleware('auth:web');
    Route::resource('user', 'UserController')->middleware('auth:web');
    Route::get('kelas/{id}/siswa', 'KelasController@kelasSiswa')->name('kelas.siswa');
    Route::post('kelas/{id}/siswa', 'KelasController@simpanSiswa')->name('kelas.siswa.store');
    Route::delete('kelas/{id}/siswa', 'KelasController@deleteKelasSiswa')->name('kelas.siswa.destroy');

    Route::resource('pengumuman', 'PengumumanController')->middleware('auth:web');
    Route::resource('materi-tugas', 'MateriTugasController');
});



