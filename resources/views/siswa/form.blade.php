@extends('layouts.app')

@section('title', 'Form siswa')

@section('content')
    <div class="header bg-primary pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Siswa</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                  <div class="row align-items-center">
                    <div class="col-12">
                      <h3 class="mb-0">Form Siswa</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($siswa['id'])) ? route('siswa.store') : route('siswa.update', $siswa['id']) }}" method="post">
                        @csrf
                        @isset($siswa['id'])
                            {{ method_field('PUT')}}
                        @endisset
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>No. Siswa</label>
                                        <input type="text" class="form-control" name="no_siswa" placeholder="" value="{{ old('no_siswa', $siswa['no_siswa']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Password Siswa</label>
                                        <input type="password" class="form-control" name="password" placeholder="" value="{{ old('password') }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                        <input type="text" class="form-control" name="nama" placeholder="" value="{{ old('nama', $siswa['nama']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="" value="{{ old('email', $siswa['email']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Tempat Lahir</label>
                                        <input type="text" class="form-control" name="tempat_lhr" placeholder="" value="{{ old('tempat_lhr', $siswa['tempat_lhr']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <input type="text" class="form-control datepicker" name="tgl_lhr" placeholder="" value="{{ old('tgl_lhr', $siswa['tgl_lhr']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label><br>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="j_kel" name="j_kel" class="custom-control-input" value="l" {{ old('j_kel', $siswa['j_kel']) == "l" ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="j_kel">Laki - Laki</label>
                                          </div>
                                          <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline2" name="j_kel" class="custom-control-input" value="p" {{ old('j_kel', $siswa['j_kel']) == "p" ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="customRadioInline2">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>No. Telp</label>
                                        <input type="number" class="form-control" name="no_telp" placeholder="" value="{{ old('no_telp', $siswa['no_telp']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea class="form-control" name="alamat" placeholder="">{{ old('alamat', $siswa['alamat']) }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="1" {{ old('status', $siswa['status']) == "1" ? 'selected' : '' }}>Aktif</option>
                                            <option value="0" {{ old('status', $siswa['status']) == "0" ? 'selected' : '' }}>Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-right">
                                        <input type="submit" value="Simpan" class="btn btn-success">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
        </div>
    </div>

@endsection
