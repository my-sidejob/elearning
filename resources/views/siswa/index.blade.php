@extends('layouts.app')

@section('title', 'Kelas')

@section('content')
    <div class="header bg-primary pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Siswa</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Siswa</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('siswa.create') }}" class="btn btn-sm btn-primary">Tambah</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>No Siswa</th>
                                    <th>Nama</th>
                                    <th>TTL</th>
                                    <th>J. Kel</th>
                                    <th>No. Telp</th>
                                    <th>Alamat</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($siswa as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->no_siswa }}</td>
                                        <td>{{ $row->nama }}</td>
                                        <td>{{ $row->tempat_lhr . ', ' . $row->tgl_lhr }}</td>
                                        <td>{{ strtoupper($row->j_kel) }}</td>
                                        <td>{{ $row->no_telp }}</td>
                                        <td>{{ $row->alamat }}</td>
                                        <td>{!! $row->getStatus() !!}</td>
                                        <td>
                                            <form action="{{ route('siswa.destroy', $row->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <a href="{{ route('siswa.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">Belum ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
