@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="header bg-primary pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Halo {{ Auth::user()->nama }}<br />Selamat Datang di Sistem E-learning SMPN 1 Baturiti</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">

    </div>

@endsection
