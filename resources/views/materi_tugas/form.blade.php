@extends('layouts.app')

@section('title')
Form {{ $type }}
@endsection

@section('content')
    <div class="header bg-primary pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">{{ ucwords($type) }} Kelas {{ $kelas->nama_kelas }}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--5">
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                  <div class="row align-items-center">
                    <div class="col-12">
                      <h3 class="mb-0">Form {{ ucwords($type) }}</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($materi_tugas['id'])) ? route('materi-tugas.store') : route('materi-tugas.update', $materi_tugas['id']) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @isset($materi_tugas['id'])
                            {{ method_field('PUT')}}
                        @endisset
                        <div class="pl-lg-4">
                            <input type="hidden" name="kelas_id" value="{{ $kelas->id }}">
                            <input type="hidden" name="type" value="{{ $type }}">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nama {{ $type }}</label>
                                        <input type="text" class="form-control" name="nama" placeholder="" value="{{ old('nama', $materi_tugas['nama']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Tanggal</label>
                                        <input type="text" class="form-control datepicker" name="tgl" placeholder="" value="{{ old('tgl', $materi_tugas['tgl']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>File</label>
                                        <input type="file" class="form-control" name="file" placeholder="" value="{{ old('file', $materi_tugas['file_'.$type]) }}">
                                        @isset($materi_tugas['id'])
                                            <p class="mt-3">{!! $materi_tugas->getFile() !!}</p>
                                        @endisset
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Deskripsi</label>
                                        <textarea type="text" class="form-control" name="deskripsi" placeholder="" rows="7">{{ old('deskripsi', $materi_tugas['deskripsi']) }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="1" {{ old('status', $materi_tugas['status']) == "1" ? 'selected' : '' }}>Aktif</option>
                                            <option value="0" {{ old('status', $materi_tugas['status']) == "0" ? 'selected' : '' }}>Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-right">
                                        <input type="submit" value="Simpan" class="btn btn-success">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
