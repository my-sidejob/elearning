@extends('layouts.app')

@section('title')
Detail {{ $type }}
@endsection

@section('content')
    <div class="header bg-primary pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Detail {{ $type }}</h6>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">{{ $materi_tugas->nama }}</h3>
                            </div>
                            <div class="col text-right">
                                {!! $materi_tugas->getStatus() !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>{{ $materi_tugas->tgl }}</p>
                        @if($materi_tugas->getFile() != 'Tidak ada file')
                            <p>{!! $materi_tugas->getFile() !!}</p>
                        @endif
                        <pre style="font-family: 'Open Sans'; line-height:1.8em; color:#525f7f;">{{ $materi_tugas->deskripsi }}</pre>
                    </div>
                </div>
            </div>
        </div>
        @if(request()->type == 'tugas')
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Jawaban Siswa</h3>
                            </div>
                            <div class="col text-right">
                                @if(Auth::guard('siswa')->check() && $jawaban->count() == 0)
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formJawaban">
                                    Tambah Jawaban
                                </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Siswa</th>
                                    <th>No Siswa</th>
                                    <th>Tanggal</th>
                                    <th>File Jawaban</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($jawaban as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->siswa->nama }}</td>
                                        <td>{{ $row->siswa->no_siswa }}</td>
                                        <td>{{ $row->tgl }}</td>
                                        <td><a href="{{ asset('storage/file_jawaban/'.$row->file_jawaban) }}" download>Download Jawaban</a></td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">Belum ada jawaban</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>

    @if(Auth::guard('siswa')->check())
    <!-- Modal -->
    <div class="modal fade" id="formJawaban" tabindex="-1" role="dialog" aria-labelledby="formJawabanLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formJawabanLabel">Tambah Siswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('siswa.jawaban', $materi_tugas->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">File Jawaban</label>
                            <input type="file" name="file_jawaban" id="" class="form-control" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
@endsection
