@extends('layouts.app')

@section('title', 'Materi & Tugas')

@section('content')
    <div class="header bg-primary pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Materi & Tugas</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--5">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <h3 class="mb-0">Kelas</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if(Auth::guard('web')->check())
                        <form action="" method="get">
                            <div class="form-group">
                                <label>Nama Kelas</label>
                                <select name="kelas_id" class="form-control select2">
                                    <option value="">- Pilih Kelas -</option>
                                    @foreach($kelas as $option)
                                        <option value="{{ $option->id }}" {{ request('kelas_id') == $option->id ? 'selected' : '' }}>{{ $option->nama_kelas }} - {{ $option->tahun_ajar }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group mb-0 text-right">
                                <input type="submit" class="btn btn-success" value="Cari">
                            </div>
                        </form>
                    @else
                        <div class="form-group">
                            <label>Nama Kelas</label>
                            <input type="text" name="" value="{{ $kelas[0]->nama_kelas }}" readonly class="form-control">
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @if(!empty(request('kelas_id')) || isset($detail_kelas))
            <div class="col-12">
                <div class="nav-wrapper">
                    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0 active" id="materi-tab" data-toggle="tab" href="#materi-tab-text" role="tab" aria-controls="materi-tab-text" aria-selected="true"><i class="fa fa-book mr-2"></i>Materi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="tugas-tab" data-toggle="tab" href="#tugas-tab-text" role="tab" aria-controls="tugas-tab-text" aria-selected="false"><i class="fa fa-clipboard-list mr-2"></i>Tugas</a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content" id="">
                    <div class="tab-pane fade show active" id="materi-tab-text" role="tabpanel" aria-labelledby="materi-tab">
                        <div class="card">
                            <div class="card-header border-0">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h3 class="mb-0">Data Materi Kelas {{ $detail_kelas->nama_kelas }}</h3>
                                    </div>
                                    <div class="col text-right">
                                        @if(Auth::guard('web')->check())
                                            <a href="{{ route('materi-tugas.create') . '?type=materi&kelas_id='.$detail_kelas->id }}" class="btn btn-sm btn-primary">Tambah</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Materi</th>
                                            <th>Tanggal</th>
                                            <th>File</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($materi as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $row->nama }}</td>
                                                <td>{{ $row->tgl }}</td>
                                                <td>{!! $row->getFile() !!}</td>
                                                <td>{!! $row->getStatus() !!}</td>
                                                <td>
                                                    @if(Auth::guard('web')->check())
                                                    <form action="{{ route('materi-tugas.destroy', $row->id) . '?type=materi&kelas_id='.$detail_kelas->id }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <a href="{{ route('materi-tugas.show', $row->id) . '?type=materi&kelas_id='.$detail_kelas->id }}" class="btn btn-sm btn-info" title="Lihat detail"><i class="fa fa-eye"></i></a>
                                                        <a href="{{ route('materi-tugas.edit', $row->id) . '?type=materi&kelas_id='.$detail_kelas->id }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                                        <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                    @else
                                                        <a href="{{ route('siswa.materi-tugas.show', $row->id) . '?type=materi&kelas_id='.$detail_kelas->id }}" class="btn btn-sm btn-info" title="Lihat detail"><i class="fa fa-eye"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="7">Belum ada data</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tugas-tab-text" role="tabpanel" aria-labelledby="tugas-tab">
                        <div class="card">
                            <div class="card-header border-0">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h3 class="mb-0">Data Tugas Kelas {{ $detail_kelas->nama_kelas }}</h3>
                                    </div>
                                    <div class="col text-right">
                                        @if(Auth::guard('web')->check())
                                            <a href="{{ route('materi-tugas.create') . '?type=tugas&kelas_id='.$detail_kelas->id }}" class="btn btn-sm btn-primary">Tambah</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Tugas</th>
                                            <th>Tanggal</th>
                                            <th>File</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($tugas as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $row->nama }}</td>
                                                <td>{{ $row->tgl }}</td>
                                                <td>{!! $row->getFile() !!}</td>
                                                <td>{!! $row->getStatus() !!}</td>
                                                <td>
                                                    @if(Auth::guard('web')->check())
                                                        <form action="{{ route('materi-tugas.destroy', $row->id) . '?type=tugas&kelas_id='.$detail_kelas->id }}" method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <a href="{{ route('materi-tugas.show', $row->id) . '?type=tugas&kelas_id='.$detail_kelas->id }}" class="btn btn-sm btn-info" title="Lihat detail"><i class="fa fa-eye"></i></a>
                                                            <a href="{{ route('materi-tugas.edit', $row->id) . '?type=tugas&kelas_id='.$detail_kelas->id }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                                            <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    @else
                                                        <a href="{{ route('siswa.materi-tugas.show', $row->id) . '?type=tugas&kelas_id='.$detail_kelas->id }}" class="btn btn-sm btn-info" title="Lihat detail"><i class="fa fa-eye"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="7">Belum ada data</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        @endif
    </div>
@endsection
