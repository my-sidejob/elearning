<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <img src="{{ asset('img/logo-smp.png') }}" class="navbar-brand-img" alt="...">
          SMPN 1 Baturiti
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}" href="{{ Auth::guard('web')->check() ? url('dashboard') : url('siswa/dashboard') }}">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('kelas') == true ) ? 'active' : '' }}" href="{{ Auth::guard('web')->check() ? route('kelas.index') : url('siswa/kelas') }}">
                <i class="fa fa-clipboard-list text-orange"></i>
                <span class="nav-link-text">Kelas</span>
              </a>
            </li>
            @if(Auth::guard('web')->check())
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('siswa') == true ) ? 'active' : '' }}" href="{{ route('siswa.index') }}">
                <i class="fa fa-users text-primary"></i>
                <span class="nav-link-text">Siswa</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('user') == true ) ? 'active' : '' }}" href="{{ route('user.index') }}">
                <i class="fa fa-user-tie text-yellow"></i>
                <span class="nav-link-text">Users</span>
              </a>
            </li>
            @endif
          </ul>
          <hr class="my-3">
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Belajar Mengajar</span>
          </h6>
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('materi-tugas') == true ) ? 'active' : '' }}" href="{{ Auth::guard('web')->check() ? url('materi-tugas') : url('siswa/materi-tugas') }}">
                <i class="fa fa-book-open text-success"></i>
                <span class="nav-link-text">Materi & Tugas</span>
              </a>
            </li>
          </ul>

          <hr class="my-3">
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Informasi</span>
          </h6>
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('pengumuman') == true ) ? 'active' : '' }}" href="{{ Auth::guard('web')->check() ? url('pengumuman') : url('siswa/pengumuman') }}">
                <i class="fa fa-chalkboard text-dark"></i>
                <span class="nav-link-text">Pengumuman</span>
              </a>
            </li>
          </ul>

          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">

            <li class="nav-item">
              <div class="nav-link active active-pro">
                <span class="nav-link-text">&copy; 2021</span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
</nav>
