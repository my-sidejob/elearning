@extends('layouts.app')

@section('title', 'Detail Pengumuman')

@section('content')
    <div class="header bg-primary pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Detail Pengumuman</h6>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">{{ $pengumuman->nama }}</h3>
                            </div>
                            <div class="col text-right">
                                {!! $pengumuman->getStatus() !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>{{ $pengumuman->tgl }}</p>
                        <pre style="font-family: 'Open Sans'; line-height:1.8em; color:#525f7f;">{{ $pengumuman->isi }}</pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
