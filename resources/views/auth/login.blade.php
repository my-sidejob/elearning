@extends('layouts.app')

@section('title', 'Login')

@section('content')
<div class="bg-default g-sidenav-show g-sidenav-pinned">
    <!-- Main content -->
    <div class="main-content">
        <!-- Header -->
        <div class="header bg-gradient-primary pt-5 pb-5">
            <div class="container">
                <div class="header-body text-center mb-7">
                    <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                            <h1 class="text-white">Login</h1>
                            <p class="text-lead text-white">Silahkan login dengan memasukan username dan password</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="separator separator-bottom separator-skew zindex-100">
                <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Form Login</small>
                            </div>
                            @if($errors->any())
                                @foreach($errors->all() as $error)
                                    <p class="text-danger">{{ $error }}</p>
                                @endforeach

                            @endif
                            <form role="form" action="{{ route('login') }}" method="post">
                                @csrf
                                <div class="form-group mb-3">
                                <div class="input-group input-group-merge input-group-alternative">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Username" type="text" name="username">
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Password" type="password" name="password">
                                </div>
                                </div>

                                <div class="text-center">
                                <button type="submit" class="btn btn-primary my-4">Sign in</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- <div class="page login-page">
    <div class="container">

        <div class="form-outer text-center d-flex align-items-center">
            <div class="form-inner w-100">
                <img src="{{ asset('img/logo-puskesmas.png') }}" width="100">
                <br><br>
                <div class="logo text-uppercase"><span></span><strong class="text-primary">LOGIN PETUGAS</strong></div>
                <p>Sistem Monitoring Tumbuh Kembang Balita.</p>
                @if($errors->any())
                    <hr>
                    @foreach($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                    @endforeach

                @endif
                <form method="post" class="text-left" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group-material">
                        <input id="login-username" type="text" name="username" required data-msg="Please enter your username" class="input-material">
                        <label for="login-username" class="label-material">Username</label>
                    </div>
                    <div class="form-group-material">
                        <input id="login-password" type="password" name="password" required="" data-msg="Please enter your password" class="input-material">
                        <label for="login-password" class="label-material">Password</label>
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" value="LOGIN" class="btn btn-primary">
                    </div>
                </form>
            </div>
            <div class="copyrights text-center">
            <p>&copy; 2019-2020 </p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
        </div>
    </div>
</div> --}}

@endsection
