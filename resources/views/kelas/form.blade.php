@extends('layouts.app')

@section('title', 'Form Kelas')

@section('content')
    <div class="header bg-primary pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Kelas</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                  <div class="row align-items-center">
                    <div class="col-12">
                      <h3 class="mb-0">Form Kelas</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($kelas['id'])) ? route('kelas.store') : route('kelas.update', $kelas['id']) }}" method="post">
                        @csrf
                        @isset($kelas['id'])
                            {{ method_field('PUT')}}
                        @endisset
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nama Kelas</label>
                                        <input type="text" class="form-control" name="nama_kelas" placeholder="Contoh: 10A, 11B" value="{{ old('nama_kelas', $kelas['nama_kelas']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Tahun Ajar</label>
                                        <input type="text" class="form-control" name="tahun_ajar" placeholder="Contoh: 2020/2021" value="{{ old('tahun_ajar', $kelas['tahun_ajar']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="1" {{ old('status', $kelas['status']) == "1" ? 'selected' : '' }}>Aktif</option>
                                            <option value="0" {{ old('status', $kelas['status']) == "0" ? 'selected' : '' }}>Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-right">
                                        <input type="submit" value="Simpan" class="btn btn-success">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
        </div>
    </div>

@endsection
