@extends('layouts.app')

@section('title', 'Kelas')

@section('content')
    <div class="header bg-primary pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Kelas</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Kelas</h3>
                            </div>
                            <div class="col text-right">
                                @if(Auth::guard('web')->check())
                                    <a href="{{ route('kelas.create') }}" class="btn btn-sm btn-primary">Tambah</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Kelas</th>
                                    <th>Tahun Ajar</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($kelas as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->nama_kelas }}</td>
                                        <td>{{ $row->tahun_ajar }}</td>
                                        <td>{!! $row->getStatus() !!}</td>
                                        <td>
                                            @if(Auth::guard('web')->check())
                                            <form action="{{ route('kelas.destroy', $row->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <a href="{{ route('kelas.siswa', $row->id) }}" class="btn btn-sm btn-info text-white" title="Edit">Siswa</a>
                                                <a href="{{ route('kelas.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                            </form>
                                            @else
                                                <a href="{{ route('siswa.kelas.detail', $row->id) }}" class="btn btn-sm btn-info text-white" title="Edit">Siswa</a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">Belum ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
