@extends('layouts.app')

@section('title', 'Kelas')

@section('content')
    <div class="header bg-primary pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Kelas {{ $kelas->nama_kelas }}</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Siswa Kelas {{ $kelas->nama_kelas }} {{ $kelas->tahun_ajar }}</h3>
                            </div>
                            <div class="col text-right">
                                <!-- Button trigger modal -->
                                @if(Auth::guard('web')->check())
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#formSiswa">
                                    Tambah Siswa
                                </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>No Siswa</th>
                                    @if(Auth::guard('web')->check())
                                    <th>Aksi</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($siswa as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->siswa->nama }}</td>
                                        <td>{{ $row->siswa->no_siswa }}</td>
                                        @if(Auth::guard('web')->check())
                                        <td>
                                            <form action="{{ route('kelas.siswa.destroy', $row->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')">Hapus Dari Kelas</button>
                                            </form>
                                        </td>
                                        @endif
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">Belum ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(Auth::guard('web')->check())
    <!-- Modal -->
    <div class="modal fade" id="formSiswa" tabindex="-1" role="dialog" aria-labelledby="formSiswaLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formSiswaLabel">Tambah Siswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('kelas.siswa.store', $kelas->id) }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="siswa">Siswa</label>
                            <select class="form-control select2" id="siswa" name="siswa_id">
                                <option>- Pilih Siswa -</option>
                                @foreach($daftar_siswa as $option)
                                    <option value="{{ $option->id }}">{{ $option->nama }} - {{ $option->no_siswa }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif
@endsection
