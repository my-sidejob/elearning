<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imunisasi extends Model
{
    protected $table = 'imunisasi';

    protected $fillable = [
        'tanggal_imunisasi',
        'keterangan',
        'keterangan',
        'jenis_imunisasi_id',
        'balita_id',
        'user_id'
    ];

    public function jenis_imunisasi()
    {
        return $this->belongsTo('App\JenisImunisasi');
    }

    public function balita()
    {
        return $this->belongsTo('App\Balita');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getDefaultValues()
    {
        return [
            'tanggal_imunisasi' => '',
            'keterangan' => '',
            'keterangan' => '',
            'jenis_imunisasi_id' => '',
            'balita_id' => '',
        ];
    }
}
