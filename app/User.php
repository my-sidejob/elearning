<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'email',
        'username',
        'password',
        'alamat',
        'no_telp',
        'tempat_lhr',
        'tgl_lhr',
        'j_kel',
        'level',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDefaultValues()
    {
        return [
            'nama' => '',
            'email' => '',
            'username' => '',
            'password' => '',
            'alamat' => '',
            'no_telp' => '',
            'tempat_lhr' => '',
            'tgl_lhr' => '',
            'j_kel' => '',
            'level' => '',
            'status' => '',
        ];
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Aktif</span>';
        } else {
            return '<span class="badge badge-warning">Tidak Aktif</span>';
        }
    }

    public function getLevel() {
        if ($this->level == "1") {
            return '<span class="badge badge-secondary">Guru</span>';
        } else {
            return '<span class="badge badge-info">Admin</span>';
        }
    }


}
