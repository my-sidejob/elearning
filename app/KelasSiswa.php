<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KelasSiswa extends Model
{
    protected $table = 'kelas_siswa';

    protected $fillable = [
        'siswa_id',
        'kelas_id',
        'status',
    ];

    public static function getDefaultValues()
    {
        return [
            'siswa_id' => '',
            'kelas_id' => '',
            'status' => '',
        ];
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Aktif</span>';
        } else {
            return '<span class="badge badge-warning">Tidak Aktif</span>';
        }
    }

    public function siswa() {
        return $this->belongsTo('App\Siswa');
    }
}
