<?php

namespace App\Http\Controllers;

use App\Jawaban;
use App\Kelas;
use App\Materi;
use App\Siswa;
use App\Tugas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class MateriTugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::guard('web')->check()){
            $data['kelas'] = Kelas::where('status', 1)->orderBy('nama_kelas', 'asc')->get();
            if(isset($request['kelas_id'])) {
                $data['materi'] = Materi::where('kelas_id', $request->kelas_id)->get();
                $data['tugas'] = Tugas::where('kelas_id', $request->kelas_id)->get();
                $data['detail_kelas'] = Kelas::findOrFail($request->kelas_id);
            }
        } else {
            $data['kelas'] = Siswa::find(Auth::user()->id)->getKelasSiswa();
            $data['materi'] = Materi::where('kelas_id', $data['kelas'][0]->id)->where('status', 1)->get();
            $data['tugas'] = Tugas::where('kelas_id', $data['kelas'][0]->id)->where('status', 1)->get();
            $data['detail_kelas'] = Kelas::findOrFail($data['kelas'][0]->id);
        }
        return view('materi_tugas.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['type'] = $request->type;
        $data['kelas'] = Kelas::findOrFail($request->kelas_id);

        if($data['type'] == 'materi') {
            $data['materi_tugas'] = Materi::getDefaultValues();
        }
        if($data['type'] == 'tugas') {
            $data['materi_tugas'] = Tugas::getDefaultValues();
        }
        return view('materi_tugas.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tgl' => 'required',
            'deskripsi' => 'required',
            'status' => 'required',
        ]);
        $input = $request->toArray();
        $type = $input['type'];
        $input['user_id'] = Auth::user()->id;
        if($request->hasFile('file')){
            $request->file('file')->store('file_'.$input['type']);
            $input['file_'.$input['type']] = $request->file('file')->hashName();
        }

        if($input['type'] == 'materi'){
            unset($input['type']);
            Materi::create($input);
            return redirect('materi-tugas?kelas_id='.$input['kelas_id'])->with('success', 'Berhasil menambah '. $type .' pada kelas');
        }
        if($input['type'] == 'tugas'){
            unset($input['type']);
            Tugas::create($input);
            return redirect('materi-tugas?kelas_id='.$input['kelas_id'])->with('success', 'Berhasil menambah '. $type .' pada kelas');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['type'] = request()->type;
        $data['kelas'] = Kelas::findOrFail(request()->kelas_id);

        if($data['type'] == 'materi') {
            $data['materi_tugas'] = Materi::findOrFail($id);
        }
        if($data['type'] == 'tugas') {
            $data['materi_tugas'] = Tugas::findOrFail($id);
            if(Auth::guard('web')->check()){
                $data['jawaban'] = Jawaban::where('tugas_id', $id)->get();
            } else {
                $data['jawaban'] = Jawaban::where('tugas_id', $id)->where('siswa_id', Auth::guard('siswa')->user()->id)->get();
            }
        }
        return view('materi_tugas.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['type'] = request()->type;
        $data['kelas'] = Kelas::findOrFail(request()->kelas_id);

        if($data['type'] == 'materi') {
            $data['materi_tugas'] = Materi::findOrFail($id);
        }
        if($data['type'] == 'tugas') {
            $data['materi_tugas'] = Tugas::findOrFail($id);
        }
        return view('materi_tugas.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'tgl' => 'required',
            'deskripsi' => 'required',
            'status' => 'required',
        ]);
        $input = $request->toArray();
        if($input['type'] == 'materi') {
            $materi_tugas = Materi::findOrFail($id);
        } else {
            $materi_tugas = Tugas::findOrFail($id);
        }
        $input['user_id'] = Auth::user()->id;
        if($request->hasFile('file')){
            Storage::delete('file_'. $input['type'] . '/' .$materi_tugas->file_.$input['type']);
            $request->file('file')->store('file_'.$input['type']);
            $input['file_'.$input['type']] = $request->file('file')->hashName();
        }
        $type = $input['type'];
        unset($input['type']);
        $materi_tugas->update($input);


        return redirect('materi-tugas?kelas_id='.$input['kelas_id'])->with('success', 'Berhasil mengubah '.$type.' pada kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(request()->type == 'materi') {
            $materi_tugas = Materi::findOrFail($id);
            Storage::delete('file_materi/'.$materi_tugas->file_materi);
        }
        if(request()->type == 'tugas') {
            $materi_tugas = Tugas::findOrFail($id);
            Storage::delete('file_tugas/'.$materi_tugas->file_tugas);
        }
        $materi_tugas->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus '.request()->type.' pada kelas');
    }

    public function submitJawaban(Request $request, $id)
    {
        $request->validate([
            'file_jawaban' => 'required'
        ]);

        $request->file('file_jawaban')->store('file_jawaban');
        $input['file_jawaban'] = $request->file('file_jawaban')->hashName();
        $input['tugas_id'] = $id;
        $input['siswa_id'] = Auth::user()->id;
        $input['tgl'] = date('Y-m-d');
        Jawaban::create($input);
        return redirect()->back()->with('success', 'Submit jawaban berhasil');

    }
}
