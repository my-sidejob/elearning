<?php

namespace App\Http\Controllers;

use App\Pengumuman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guard('web')->check()){
            $data['pengumuman'] = Pengumuman::orderBy('tgl', 'desc')->get();
        } else {
            $data['pengumuman'] = Pengumuman::where('status', 1)->orderBy('tgl', 'desc')->get();
        }
        return view('pengumuman.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pengumuman'] = Pengumuman::getDefaultValues();

        return view('pengumuman.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tgl'=> 'required',
            'isi' => 'required',
            'status' => 'required',
        ]);

        $input = $request->toArray();
        $input['user_id'] = Auth::user()->id;
        Pengumuman::create($input);
        return redirect()->route('pengumuman.index')->with('success', 'Berhasil menambah data pengumuman');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pengumuman  $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['pengumuman'] = Pengumuman::findOrFail($id);
        return view('pengumuman.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pengumuman  $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pengumuman'] = Pengumuman::findOrFail($id);
        return view('pengumuman.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pengumuman  $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'tgl'=> 'required',
            'isi' => 'required',
            'status' => 'required',
        ]);
        $input = $request->toArray();
        $input['user_id'] = Auth::user()->id;
        Pengumuman::findOrfail($id)->update($input);
        return redirect()->route('pengumuman.index')->with('success', 'Berhasil mengubah data pengumuman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pengumuman  $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pengumuman::findOrfail($id)->delete();
        return redirect()->route('pengumuman.index')->with('success', 'Berhasil menghapus data pengumuman');
    }
}
