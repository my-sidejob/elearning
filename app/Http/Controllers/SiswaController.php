<?php

namespace App\Http\Controllers;

use App\Siswa;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['siswa'] = Siswa::orderBy('nama', 'asc')->get();
        return view('siswa.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['siswa'] = Siswa::getDefaultValues();

        return view('siswa.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_siswa' => 'required|unique:siswa,no_siswa',
            'password' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'tempat_lhr' => 'required',
            'tgl_lhr' => 'required',
            'j_kel' => 'required',
            'no_telp' => 'required',
            'alamat' => 'required',
            'status' => 'required',
        ]);
        $input = $request->toArray();
        $input['password'] = bcrypt($input['password']);
        Siswa::create($input);
        return redirect()->route('siswa.index')->with('success', 'Berhasil menambah data siswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['siswa'] = Siswa::findOrFail($id);
        return view('siswa.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'no_siswa' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'tempat_lhr' => 'required',
            'tgl_lhr' => 'required',
            'j_kel' => 'required',
            'no_telp' => 'required',
            'alamat' => 'required',
            'status' => 'required',
        ]);
        $input = $request->toArray();
        if(empty($input['password'])) {
            unset($input['password']);
        } else {
            $input['password'] = bcrypt($input['password']);
        }
        Siswa::findOrfail($id)->update($input);
        return redirect()->route('siswa.index')->with('success', 'Berhasil mengubah data siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Siswa::findOrfail($id)->delete();
        return redirect()->route('siswa.index')->with('success', 'Berhasil menghapus data siswa');
    }
}
