<?php

namespace App\Http\Controllers;

use App\Kelas;
use App\KelasSiswa;
use App\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guard('web')->check()){
            $data['kelas'] = Kelas::orderBy('tahun_ajar', 'desc')->get();
        } else {
            $data['kelas'] = Siswa::find(Auth::user()->id)->getKelasSiswa();
        }
        return view('kelas.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kelas'] = Kelas::getDefaultValues();

        return view('kelas.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kelas' => 'required|unique:kelas,nama_kelas',
            'tahun_ajar'=> 'required',
            'status' => 'required',
        ]);

        Kelas::create($request->toArray());
        return redirect()->route('kelas.index')->with('success', 'Berhasil menambah data kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function show(Kelas $kelas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kelas'] = Kelas::findOrFail($id);
        return view('kelas.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kelas' => 'required',
            'tahun_ajar'=> 'required',
            'status' => 'required',
        ]);

        Kelas::findOrfail($id)->update($request->toArray());
        return redirect()->route('kelas.index')->with('success', 'Berhasil mengubah data kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kelas::findOrfail($id)->delete();
        return redirect()->route('kelas.index')->with('success', 'Berhasil menghapus data kelas');
    }

    public function kelasSiswa($id) {
        $data['kelas'] = Kelas::findOrfail($id);
        $data['siswa'] = KelasSiswa::where('kelas_id', $id)->get();
        $data['daftar_siswa'] = Siswa::getAvailableSiswa();
        return view('kelas.siswa', $data);
    }

    public function simpanSiswa(Request $request, $id)
    {
        $request->validate([
            'siswa_id' => 'required'
        ]);

        KelasSiswa::create([
            'kelas_id' => $id,
            'siswa_id' => $request->siswa_id,
            'status' => 1,
        ]);

        return redirect()->back()->with('success', 'Berhasil menambah data siswa ke kelas');

    }

    public function deleteKelasSiswa($id) {
        KelasSiswa::findOrFail($id)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus siswa dari kelas');
    }
}
