<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tugas extends Model
{
    protected $table = 'tugas';

    protected $fillable = [
        'nama',
        'tgl',
        'deskripsi',
        'file_tugas',
        'status',
        'kelas_id',
        'user_id'
    ];

    public static function getDefaultValues()
    {
        return [
            'nama' => '',
            'tgl' => date('Y-m-d'),
            'deskripsi' => '',
            'file_tugas' => '',
            'status' => '',
            'kelas_id' => '',
            'user_id' => ''
        ];
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Aktif</span>';
        } else {
            return '<span class="badge badge-warning">Tidak Aktif</span>';
        }
    }

    public function getFile() {
        if(!empty($this->file_tugas)) {
            return '<a href="'.asset('storage/file_tugas/'.$this->file_tugas).'" target="_blank" download>Download File</a>';
        } else {
            return "Tidak ada file";
        }
    }
}
