<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengumuman extends Model
{
    protected $table = 'pengumuman';

    protected $fillable = [
        'nama',
        'tgl',
        'isi',
        'status',
        'user_id',
    ];

    public static function getDefaultValues()
    {
        return [
            'nama' => '',
            'tgl' => date('Y-m-d'),
            'isi' => '',
            'status' => '',
            'user_id' => '',
        ];
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Aktif</span>';
        } else {
            return '<span class="badge badge-warning">Tidak Aktif</span>';
        }
    }
}
