<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    protected $table = 'materi';

    protected $fillable = [
        'nama',
        'tgl',
        'deskripsi',
        'file_materi',
        'status',
        'kelas_id',
        'user_id'
    ];

    public static function getDefaultValues()
    {
        return [
            'nama' => '',
            'tgl' => date('Y-m-d'),
            'deskripsi' => '',
            'file_materi' => '',
            'status' => '',
            'kelas_id' => '',
            'user_id' => ''
        ];
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Aktif</span>';
        } else {
            return '<span class="badge badge-warning">Tidak Aktif</span>';
        }
    }

    public function getFile() {
        if(!empty($this->file_materi)) {
            return '<a href="'.asset('storage/file_materi/'.$this->file_materi).'" target="_blank" download>Download File</a>';
        } else {
            return "Tidak ada file";
        }
    }
}
