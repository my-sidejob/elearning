<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Siswa extends Authenticatable
{
    use Notifiable;

    protected $table = 'siswa';

    protected $fillable = [
        'no_siswa',
        'password',
        'nama',
        'email',
        'tempat_lhr',
        'tgl_lhr',
        'j_kel',
        'no_telp',
        'alamat',
        'status',
    ];

    public static function getDefaultValues()
    {
        return [
            'no_siswa' => '',
            'password' => '',
            'nama' => '',
            'email' => '',
            'tempat_lhr' => '',
            'tgl_lhr' => '',
            'j_kel' => '',
            'no_telp' => '',
            'alamat' => '',
            'status' => '',
        ];
    }

    public function getStatus() {
        if ($this->status == "1") {
            return '<span class="badge badge-success">Aktif</span>';
        } else {
            return '<span class="badge badge-warning">Tidak Aktif</span>';
        }
    }

    public static function getAvailableSiswa() {
        $kelas = Kelas::where('status', 1)->get()->pluck('id')->all();
        $kelas_siswa = KelasSiswa::whereIn('kelas_id', $kelas)->get()->pluck('siswa_id')->all();
        return Siswa::whereNotIn('id', $kelas_siswa)->where('status', 1)->orderBy('nama', 'asc')->get();
    }

    public function getKelasSiswa() {
        $kelas_siswa = KelasSiswa::where('siswa_id', $this->id)->join('kelas', 'kelas.id', 'kelas_siswa.kelas_id')->orderBy('tahun_ajar', 'desc')->limit(1)->get();
        return $kelas_siswa;
    }
}
