<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = 'jawaban';

    protected $fillable = [
        'siswa_id',
        'tugas_id',
        'tgl',
        'file_jawaban',
    ];

    public static function getDefaultValues()
    {
        return [
            'siswa_id' => '',
            'tugas_id' => '',
            'tgl' => '',
            'file_jawaban' => '',
        ];
    }


    public function siswa() {
        return $this->belongsTo('App\Siswa');
    }
}
