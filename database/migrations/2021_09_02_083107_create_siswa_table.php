<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_siswa')->unique();
            $table->string('password');
            $table->string('nama');
            $table->string('email')->nullable();
            $table->string('tempat_lhr');
            $table->date('tgl_lhr');
            $table->enum('j_kel', ['l', 'p']);
            $table->string('alamat');
            $table->string('no_telp');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
