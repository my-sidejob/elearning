<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 20; $i++){
            DB::table('siswa')->insert([
                'no_siswa' => $faker->randomNumber(),
                'password' => bcrypt('secret'),
                'nama' => $faker->name,
                'email' => $faker->email,
                'tempat_lhr' => $faker->city,
                'tgl_lhr' => $faker->date('Y-m-d'),
                'j_kel' => 'L',
                'no_telp' => $faker->randomNumber(),
                'alamat' => $faker->address,
                'status' => '1',
            ]);
        }
    }
}
